const logReceipt = function logReceipt(){
    let sum = 0;
    let i = 0;
    let tax = 0;
    const args = Array.from(arguments);
    args.forEach(function(item) {
        console.log(item)
        if (i==0){
            tax = item;
        }
        else {
            console.log(`${item.descr} - $${item.price}`);
            sum += item.price;
        }   
        i++;
    });
    let tax_amount = sum * tax;
    let tax_amount_str = (sum * tax).toFixed(2)
    let total = (sum + tax_amount).toFixed(2);
    console.log(`Subtotal - $${sum}`)
    console.log(`Tax - $${tax_amount_str}`)
    console.log(`Total - $${total}`)
  }

const bud = {descr: 'Bud Light', price: 3.99};
const burger = {descr: 'Hamburger', price: 6.99};
logReceipt(
    0.1,
    {descr: 'Burrito', price: 5.99},
    {descr: 'Chips & Salsa', price: 2.99},
    {descr: 'Sprite', price: 1.99}
  );

logReceipt(0.1, bud, burger);

// Bud Light - $3.99
// Hamburger - $6.99
// Total - $10.98