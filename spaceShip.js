// Create a constructor function SpaceShip
// - should set two properties: name and topSpeed
// - should have a method accelerate that logs to the console 
//   `${name} moving to ${topSpeed}`
const spaceShip = function spaceShip(shipName, shipTopSpeed) {
    let name = shipName;
    let topSpeed = shipTopSpeed;
    this.accelerate = function() {
      //const {name, topSpeed} = this;
      //const name = this.name;
      //const topSpeed = this.topSpeed;
      console.log(`${name} moving to ${topSpeed}`);
    };
    this.changeTopSpeed = function(newTopSpeed) {topSpeed = newTopSpeed;}
  };


// Make name and topSpeed private

// Keep both name and topSpeed private, but 
// add a method that changes the topSpeed


// Call the constructor with a couple ships, change the topSpeed
// using the method, and call accelerate.

const spaceShip1 = new spaceShip('Enterprise', 'Warp 9');
const spaceShip2 = new spaceShip('Excelsior', 'Warp 3');
const spaceShip3 = new spaceShip('Defiant', 'Warp 10');

spaceShip1.accelerate();
spaceShip1.changeTopSpeed(9.9);
spaceShip1.accelerate();
spaceShip2.accelerate();
spaceShip3.accelerate();