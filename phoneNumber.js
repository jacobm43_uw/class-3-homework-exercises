function testPhoneNumber(phoneNumber) {
    let regex1 = /^\(?\d{3}\)?[-\s]?\d{3}[-\s]?\d{4}/;
    return regex1.test(phoneNumber);
  };
 function parsePhoneNumber(phoneNumber) {
    let phoneObj = {};
    let areaCodeRegex = /\d{3}/;
    array1 = areaCodeRegex .exec(phoneNumber);
    let phoneRegex = /\d{3}(?:\s|-)\d{4}$/;
    array2 = phoneRegex.exec(phoneNumber);

    console.log(array1[0]);
    console.log(array2[0]);

    phoneObj['areaCode'] = array1[0];
    phoneObj['phoneNumber'] = array2[0];
    return phoneObj;
 };

console.log( testPhoneNumber('(206) 333-4444') );
console.log( parsePhoneNumber('(206) 333-4444') );
console.log( testPhoneNumber('206-333-4444') );
console.log( parsePhoneNumber('206-333-4444') );
console.log( testPhoneNumber('206 333 4444') );
console.log( parsePhoneNumber('206 333 4444') );